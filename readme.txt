Helsekompetanse API Drupal wrapper module
Library URL: https://bitbucket.org/Helsekompetanse/hkapi/raw/master/hkapi.classes.php




Sample usage:

<?php

// @todo add api resources alias to aegir

include "hkapi.classes.php";

/**
 * Class MockupDrupal
 * Mockup System class for the hkapi client library.
 */
class MockupDrupal implements Helsekompetanse\api\systemwrapper {
  public static function errorlog($type, $message, $variables = array(), $severity = 3, $link = NULL) {
    echo "ERROR $type $message ".var_export($variables,TRUE)." $severity $link\n";
  }
  public static function getHeaders() {
    return array('From' => "http://profil.helsekompetanse.no/user/7", 'Referer' => 'http://kurs.helsekompetanse.no/borgestestrom/');
  }
  public static function getCredentials() {
    return array('key' => 'mysecretkey', 'secret' => 'mysecretsecret'); // localhost
  }
}

// Override System class (defaults to Drupal implementation)
\Helsekompetanse\api\System::$system = '\MockupDrupal';

//$result = Helsekompetanse\api\Course::create(array('title' => 'Mitt flotte kurs','url' => 'http://kurs.helsekompetanse.no/borgestestrom','meta' => array('legacy_id' => 1649)));
$result = Helsekompetanse\api\User::search(array('havard.pedersen@telemed.no', 'sfjoa@sdf.com'));

// Check for fail condition. Errors have already been logged using the system class above.
if ($result === FALSE) {
  echo "FAIL! ".var_export(Helsekompetanse\api\OAuth::lastDebug(),TRUE). "\n";
} else {
  echo "SUCCESS! ".var_export($result, TRUE)."\n";
}

// var_export(Helsekompetanse\api\OAuth::lastDebug());
