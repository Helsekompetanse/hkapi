<?php

namespace Helsekompetanse\api;

use Exception;

/**
 * Class Course
 * The hk_course resource
 */
class Course extends Resource {

  /**
   * @param array $payload Request data
   * @return bool|mixed Unserialized data or FALSE on failure
   */
  public static function create($payload) {
    return self::post("hk_course.json", $payload);
  }

  /**
   * @param int $nid  course_id of course to retrieve from profile
   * @return bool|mixed Unserialized data or FALSE on failure
   */
  public static function retrieve($nid) {
    return self::get("hk_course/".trim($nid.".json"));
  }

  /**
   * Case insensitive search for course from its title.
   *
   * @param string $title with course title to look up
   * @param bool $exact_match only return if case insensitive exact match.
   * @return bool|mixed Unserialized data or FALSE on failure
   */
  public static function search($title, $exact_match=FALSE) {
    $params = array();
    $params['title'] = json_encode(strtolower((string) $title));
    $params['exact_match'] = json_encode((bool) $exact_match);
    return self::get("hk_course.json", $params);
  }
}

/**
 * Class User
 * The hk_user resource
 */
class User extends Resource {

  /**
   * @param array $mails Array of mail addresses to look up
   * @return bool|mixed Unserialized data or FALSE on failure
   */
  public static function search($mails = NULL, $roles = NULL) {
    $params = array();
    if (!empty($mails)) {
      $params['mails'] = json_encode($mails);
    }
    if (!empty($roles)) {
      $params['roles'] = json_encode($roles);
    }
    return self::get("hk_user.json", $params);
  }

  /**
   * @param int $uid User id of user to retrieve from profile
   * @return bool|mixed Unserialized data or FALSE on failure
   */
  public static function retrieve($uid) {
    return self::get("hk_user/".trim($uid).".json");
  }

  /**
   * @param array $user User definition
   * @return bool|mixed Unserialized data or FALSE on failure
   */
  public static function create($user) {
    return self::post("hk_user.json", $user);
  }
}

/**
 * Class Membership
 * The hk_membership resource
 */
class Membership extends Resource {

  /**
   * @param int $cid_uid Combined cource_id and user_id to get membership info from
   * @return bool|mixed Unserialized data or FALSE on failure
   */
  public static function retrieve($cid_uid) {
    return self::get("hk_membership/".trim($cid_uid).".json");
  }

  /**
   * @param array $membership Membership definition
   * @return bool|mixed Unserialized data or FALSE on failure
   */
  public static function create($membership) {
    return self::post("hk_membership.json", $membership);
  }
}

/**
 * Class Resource
 * A base resource class for hkapi.
 */
class Resource {

  public static function post($dest, $payload) {
    $oauth = System::getOAuth();
    return $oauth->fetch(OAuth::$url.$dest, $payload, OAUTH_HTTP_METHOD_POST);
  }
// @todo how to pass get params
  public static function get($dest, $payload = array()) {
    $oauth = System::getOAuth();
    return $oauth->fetch(OAuth::$url.$dest, $payload, OAUTH_HTTP_METHOD_GET);
  }
}

/**
 * Class OAuth
 * The OAuth wrapper class.
 */
class OAuth extends \OAuth {

  /**
   * The URL for our API
   * @var string
   */
  public static $url = 'http://profil.helsekompetanse.no/rest/v1/';

  public $lastException = NULL;

  /**
   * Does a HTTP request using the OAuth class, handling exceptions and unserializing.
   * @param string $resource URL
   * @param array $payload Array with request data (for POST)
   * @param integer $method OAUTH constant for HTTP method
   * @param array $headers Array of extra headers to ship
   * @return bool|mixed Unserialized data or FALSE on failure.
   */
  public function fetch($resource, $payload = array(), $method = OAUTH_HTTP_METHOD_GET, $headers = NULL) {
    $this->lastException = NULL; // Empty exception store

    if ($method != OAUTH_HTTP_METHOD_GET) {
      $payload = json_encode($payload);
    }

    if  (!is_array($headers)) {
      $headers = array_merge(System::getHeaders(), array('Content-Type' => 'application/json'));
    }

    try {
      $res = parent::fetch($resource, $payload, $method, $headers);
    } catch (Exception $e) { // HTTP codes starting with 4 and 5 causes exceptions
      $this->lastException = $e;
      $params = array(
        '@method' => OAuth::lookupMethod($method),
        '@resource' => $resource,
        '@payload' => print_r($payload, TRUE),
        '@headers' => print_r($headers, TRUE),
        '@result' => $e->getMessage()
      );
      System::errorlog('hkapi', 'Failed doing a @method at @resource with @payload and headers @headers. Result: @result', $params, 3);
      return FALSE;
    }
    if (!$res) { // A blank result is considered an error. Successful calls should always return _something_.
      $params = array('@method' => OAuth::lookupMethod($method), '@resource' => $resource, '@payload' => var_export($payload,TRUE), '@headers' => var_export($headers,TRUE));
      System::errorlog('hkapi', 'Failed doing a @method at @resource with @payload and headers @headers. No result.', $params, 3);
      return FALSE;
    }
    $return = json_decode($this->getLastResponse(),TRUE);

    if ($return === NULL) {
      System::errorlog('hkapi', 'Failed decoding result @result', array('result'=>$this->getLastResponse()), 3);
      return FALSE;
    }

    return $return;
  }

  /**
   * Get debug information about last request
   * @return array
   */
  public function getLastDebug() {
    return array(
      'lastresponse' => $this->getLastResponse(),
      'lastresponseheaders' => $this->getLastResponseHeaders(),
      'lastresponseinfo' => $this->getLastResponseInfo(),
      'debug' => $this->debugInfo,
    );
  }

  public static function lastDebug() {
    $oauth = System::getOAuth();
    return $oauth->getLastDebug();
  }

  /**
   * Look up OAuth HTTP verbs.
   * @param $method
   * @return string
   */
  public static function lookupMethod($method) {
    $lookup = array(
      OAUTH_HTTP_METHOD_GET => "GET",
      OAUTH_HTTP_METHOD_POST => "POST",
      OAUTH_HTTP_METHOD_PUT => "PUT",
      OAUTH_HTTP_METHOD_DELETE => "DELETE",
      OAUTH_HTTP_METHOD_HEAD => "HEAD",
    );
    if (isset($lookup[$method])) {
      return $lookup[$method];
    } else {
      return 'UNKNOWN';
    }
  }

  /**
   * Returns the last HTTP response code
   * @return integer
   */
  public function lastResponseCode() {
    if (method_exists($this, 'getLastResponseInfo') && ($responseinfo = $this->getLastResponseInfo())) {
      return $responseinfo['http_code'];
    }

    $headers = OAuth::parseHeaders($this->getLastResponseHeaders());
    return $headers['_status'];
  }



  /**
   * Parse a set of HTTP headers
   *
   * @param array $headers The php headers to be parsed
   * @param string $header The name of the header to be retrieved
   * @return array|string A header value if a header is passed;
   *         An array with all the headers otherwise
   */
  public static function parseHeaders(array $headers, $header = NULL) {
    if ('HTTP' === substr($headers[0], 0, 4)) {
      $output = array();
      list($output['_httpv'], $output['_status'], $output['_statustext']) = explode(' ', $headers[0]);
      unset($headers[0]);
    } else {
      $output = array('_httpv' => '', '_status' => FALSE, '_statustext' => 'No status from server');
    }

    foreach ($headers as $v) {
      $h = preg_split('/:\s*/', $v);
      $output[strtolower($h[0])] = $h[1];
    }

    if ($header !== NULL) {
      if (isset($output[strtolower($header)])) {
        return $output[strtolower($header)];
      }
      return NULL;
    }

    return $output;
  }

}

/**
 * Interface systemwrapper
 * @package Helsekompetanse\api
 */
interface systemwrapper
{
  /**
   * Logger. Based on Drupal 6 and 7's watchdog().
   * @param string $type
   * @param string $message
   * @param array $variables
   * @param int $severity defaults to 3, which is WATCHDOG_ERROR
   * @param null $link
   */
  public static function errorlog($type, $message, $variables = array(), $severity = 3, $link = NULL);

  /**
   * Obtain headers to be used for communication
   * @return mixed
   */
  public static function getHeaders();

  /**
   * Get credentials.
   * @return array|null Credentials array consisting of two elements, 'key' and 'secret'.
   */
  public static function getCredentials();

}

class System implements systemwrapper {
  /**
   * The system class for communicating with the CMS
   * @var string
   */
  public static $system = '\Helsekompetanse\api\DrupalSystem';

  public static function errorlog($type, $message, $variables = array(), $severity = 3, $link = NULL) {
    $class = self::$system;
    return $class::errorlog($type, $message, $variables, $severity, $link);
  }

  public static function getHeaders() {
    $class = self::$system;
    return $class::getHeaders();
  }

  public static function getCredentials() {
    $class = self::$system;
    return $class::getCredentials();
  }

  /**
   * Static cache of OAuth object.
   * @return OAuth object
   */
  public static function getOAuth() {
    $credentials = System::getCredentials();
    static $oauth = NULL;
    if (!$oauth) {
      $oauth = new OAuth($credentials['key'], $credentials['secret'], OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
    }
    return $oauth;
  }


}

/**
 * Class DrupalSystem
 */
class DrupalSystem implements systemwrapper {
  /**
   * @param string $type
   * @param string $message
   * @param array $variables
   * @param int $severity
   * @param null $link
   */
  public static function errorlog($type, $message, $variables = array(), $severity = 3, $link = NULL) {
    watchdog($type, $message, $variables, $severity, $link);
  }

  /**
   * Build the headers recommended by hkapi. This function is probably not only Drupal-specific, but helsekompetanse specific.
   * @return array of HTTP headers
   */
  public static function getHeaders() {
    $headers = array();
    global $user;
    $user_init = is_object($user) && property_exists($user, 'init') ? $user->init : '';
    $match = preg_match('/profil.helsekompetanse.no\/user\/(\d+)\/edit/i', $user_init, $result);
    if ($match) {
      $headers['From'] = "http://profil.helsekompetanse.no/user/{$result[1]}";
    }
    $headers['Referer'] = url(NULL,array('absolute'=>TRUE));
    return $headers;
  }

  /**
   * Set or get credentials from variables.
   * @return array|null Credentials array consisting of two elements, 'key' and 'secret'.
   */
  public static function getCredentials() {
    $credentials = array(
      'key' => variable_get('hkapi_key', ''),
      'secret' => variable_get('hkapi_secret', ''),
    );

    //Check if credentials was set, and write error log if not.
    if ($credentials['key']=='' || $credentials['secret']=='') {
      global $base_url;
      System::errorlog("hkapi","Variables hkapi_key and hlapi_secret are not set correctly on $base_url");
    }
    return $credentials;

  }
}
